import random


def guess_int(self, start, stop):
    return random.randint(start, stop)


def guess_float(self, start, stop):
    return random.uniform(start, stop)
