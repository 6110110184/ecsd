import guess_num.guess_number as guess_number
import unittest
from unittest import mock


class TestGuessNumber(unittest.TestCase):
    @mock.patch('random.randint', return_value=4)
    def test_random_int_positive(self, random_randint):
        numbermin = mock.MagicMock()
        numbermax = mock.MagicMock()
        self.assertEqual(guess_number.guess_int(self, numbermin, numbermax), 4)

    @mock.patch('random.randint', return_value=-54)
    def test_random_int_negative(self, random_randint):
        numbermin = mock.MagicMock()
        numbermax = mock.MagicMock()
        self.assertEqual(guess_number.guess_int(
            self, numbermin, numbermax), -54)

    @mock.patch('random.uniform', return_value=3.14)
    def test_random_float_positive(self, random_uniform):
        numbermin = mock.MagicMock()
        numbermax = mock.MagicMock()
        self.assertEqual(guess_number.guess_float(
            self, numbermin, numbermax), 3.14)

    @mock.patch('random.uniform', return_value=-839.0985)
    def test_random_float_negative(self, random_uniform):
        numbermin = mock.MagicMock()
        numbermax = mock.MagicMock()
        self.assertEqual(guess_number.guess_float(
            self, numbermin, numbermax), -839.0985)

    @mock.patch('random.randint', return_value=4895261)
    def test_random_int_a_lot_value(self, random_randint):
        numbermin = mock.MagicMock()
        numbermax = mock.MagicMock()
        self.assertEqual(guess_number.guess_int(
            self, numbermin, numbermax), 4895261)

    @unittest.expectedFailure
    def test_random_int_max_before_min(self):
        self.assertEqual(guess_number.guess_int(self, 11, 10), 11)

    @mock.patch('random.randint', return_value=-11)
    def test_random_int_base_negetive(self, random_randint):
        self.assertEqual(guess_number.guess_int(self, -20, -4), -11)

    @mock.patch('random.uniform', return_value=-320.2465)
    def test_random_float_base_negetive(self, random_randint):
        self.assertEqual(guess_number.guess_float(
            self, -500, -100), -320.2465)
